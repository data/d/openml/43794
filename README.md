# OpenML dataset: Tweets-with-keyword-lockdown-in-April-July-2020

https://www.openml.org/d/43794

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
This data was collected to be used with an academic project of mine. The project was about sentiment analysis of tweets during lockdown.
Content
I used the GetOldTweets3 (https://pypi.org/project/GetOldTweets3/) python3 library to pull the tweets off Twitter. The tweets range between 1 April 2020 to 1 August 2020, which was the peak lockdown period in India. Tweets with duplicate text and NaN values and that was the only cleaning I did on the data.
Total rows of tweets: 95488
Columns:

Index (be sure to use df = pandas.read_csv("tweets_lockdown.csv", index_col=0))
Text - The text of the tweet
Date - Date and time of tweet in datetime format
Retweets - Number of retweets for the tweet
Favorites - Favorites on the tweet
Mentions - Usernames mentioned in the tweets in  format
HashTags - Hashtags present in the tweet in  format

"Top Tweets" attribute was turned off while scraping.
Inspiration
Twitter data gives us a lot of scope for data cleaning, text preprocessing, association rule mining, sentiment analysis and so on.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43794) of an [OpenML dataset](https://www.openml.org/d/43794). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43794/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43794/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43794/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

